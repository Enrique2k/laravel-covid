<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Http\Resources\ia14Resource;
use App\Http\Resources\ShowResource;
use App\Models\ia14;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Ia14Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $ia14 = new ia14();
        $ia14 ->fecha = $request->fecha;
        $ia14 ->ccaas_id = $request->ccaas_id;
        $ia14 ->incidencia = $request->incidencia;

        $ia14->save();

        return response()->json($ia14);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return ShowResource
     */
    public function show($id)
    {
        $ia14 = ia14::where('fecha', $id)->first();
        if (!$ia14){
            return response()->json(['errors'=>Array(['code'=> 404, 'message'=>'No existe la fecha'])], 404);
        }
        return new ShowResource($ia14);
    }

    public function showCollection($id, $id2)
    {
        if ($id>$id2){
            return response()->json(['errors'=>Array(['code'=> 404, 'message'=>'Primera fecha superior a la segunda'])], 404);
        }
        $ia14 = DB::select(DB::raw("SELECT * FROM ia14 WHERE fecha BETWEEN '$id' and '$id2'"));
        if (!$ia14){
            return response()->json(['errors'=>Array(['code'=> 404, 'message'=>'No existe la fecha'])], 404);
        }
        return new CovidCollection($ia14);
    }

    public function showAll()
    {
        $ia14 = ia14::all();
        if (!$ia14){
            return response()->json(['errors'=>Array(['code'=> 404, 'message'=>'No hay datos'])], 404);
        }
        return response()->json(['status'=>'ok', 'data'=>$ia14], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // TENGO QUE REALIZAR EL UPDATE Y DELETE
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
