<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CasosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $casos = DB::select(DB::raw("SELECT * FROM casos WHERE fecha='$id'"));
        if (!$casos){
            return response()->json(['errors'=>Array(['code'=> 404, 'message'=>'No existe la fecha'])], 404);
        }
        return response()->json(['status'=>'ok', 'data'=>$casos], 200);

    }

    public function showCollection($id, $id2)
    {
        if ($id>$id2){
            return response()->json(['errors'=>Array(['code'=> 404, 'message'=>'Primera fecha superior a la segunda'])], 404);
        }
        $casos = DB::select(DB::raw("SELECT * FROM casos WHERE fecha BETWEEN '$id' and '$id2'"));
        if (!$casos){
            return response()->json(['errors'=>Array(['code'=> 404, 'message'=>'No existe la fecha'])], 404);
        }
        return new CovidCollection($casos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
